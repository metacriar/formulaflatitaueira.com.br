<!-- BEGIN PAGE CONTENT -->
<div class="container reservation-page-wrap">
    <div id="roow-par-wrap">
        <h3 style="padding-top: 50px;">SUA RESERVA SERÁ CONFIRMADA CONFORME DISPONIBILIDADE DO HOTEL</h3>
        <form action="./">
    
            <div id="roow-description-wrap" style="font-size: 16px;">
                Precisamos de algum tempo para analisar o seu pedido (geralmente leva apenas algumas horas). Depois de fazer isso, nós lhe enviaremos uma mensagem de retorno, para concluimos a reserva.
        </div>
         <button type="submit" class="room-reservation-select">Voltar a Pagina Inicial</button>

        </form>
  
    </div>
</div>    


<!-- END PAGE CONTENT -->