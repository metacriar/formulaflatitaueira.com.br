<!DOCTYPE html>
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class=""> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   

        <title>Hotel Fórmula Flat Itaueira</title>

        <title>Hotel Executive Flat</title>
         <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="Teresina, PI, ,Piaui, hotel, hotéis baratos, hospedagem, acomodações, hotel fazenda, resorts, hoteis com desconto, ousadas em Teresina, hotéis em Teresina, hotéis, flat, apart-hotel, pousada, aubergues, hotel, hospedagem, onde ficar, acomodações"/>
        <meta name="description" content="O Arrey Express conta com 6 andares e 170 apartamentos luxo com ar-condicionado, TV de LED a cabo, cofre eletrônico, frigobar, acesso a internet e telefone."/>
        <meta name="robots" content="index,follow" />
        <meta property="og:locale" content="pt_BR" />
        <meta property="og:title" content="Hotel Express Arrey - Praticidade e economia se hospedam aqui" />
        <meta property="og:description" content="O Arrey Express conta com 6 andares e 170 apartamentos luxo com ar-condicionado, TV de LED a cabo, cofre eletrônico, frigobar, acesso a internet e telefone." />
        <meta property="og:site_name" content="Hotel Express Arrey" />
		
	


        <!-- GOOGLE WEB FONT -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,800' rel='stylesheet' type='text/css'>

        <!-- NATION THEME STYLE -->
        <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" media="screen">

        <!-- REVOLUTION SLIDER STYLE -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/settings.css" media="screen" />

        <!-- GOOGLE MAPS API -->
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCbEJ826CbvSGwFTtO-KTWoowpdn3GxF50&sensor=false"></script>

        <!-- BOOKING CALENDAR STYLE -->
        <link href="<?php echo base_url() ?>assets/css/booking-calendar-pro.css" rel="stylesheet" media="screen">
        
        <link href="<?php echo base_url() ?>assets/css/switcher.css" rel="stylesheet" media="screen">

        <!--[if lt IE 9]>
        <script src="../assets/js/html5shiv.js"></script>
        <script src="../assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="to-top">
		<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-57077676-1', 'auto');
			  ga('send', 'pageview');

                </script>
        <div id="wrapper">
            <!-- BEGIN HEADER -->
           
            <header id="main-page-header-wrap">
                <!-- BEGIN TOP INFO BAR -->
                <div id="headcontainer">
                    <div id="top-sticky-bar" class="container">
                        <div id="sticky-top-bar">
                            <div id="top-contact-wrap">
                                <!-- BEGIN TOP CONTACT INFO -->
                                <div id="top-street-address"><span class="icon-map-marker"></span>  Rua Regeneração N° 469, Ilhotas - Teresina/PI - Brasil</div>
                                <div id="top-phone"><span class="icon-mobile-phone"></span>+55(86)3216 6700</div>
                                <div id="top-email"><span class="icon-envelope-alt"></span> reservas@executiveflat.com.br</div>
                                <div id="top-email"><a href="apartamento" style="color: #fff; text-decoration: none; margin-left: 20px;">RESERVAS ONLINE</a></div>
                                <!-- END TOP CONTACT INFO 
                                <div id="top-search"><span class="icon-search"></span>
                                        <div id="top-search-window-wrap"><input type="text" value="Procurar.." /></div>
                                </div>
                                <!-- BEGIN TOP LANGUAGES SELECT -->
<!--                                <div id="top-language-select">
                                    <ul class="dropdown">
                                        <li><a href="#"><img src="<?php echo base_url() ?>assets/images/br.png" class="country-flag"> Português <span class="icon-angle-down"></span></a>
                                            <ul class="sub_menu">
                                                <li><a href="#"><img src="<?php // echo base_url()  ?>assets/images/gb.png" class="country-flag"> English</a></li>
                                                <li><a href="#"><img src="<?php // echo base_url()  ?>assets/images/es.png" class="country-flag"> Spanish</a></li>
                                            </ul> 
                                        </li>
                                    </ul>
                                </div>-->
                                <!-- END TOP LANGUAGES SELECT -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END TOP INFO BAR -->
                
                <!-- BEGIN LOGO AND NAVIGATION WRAP -->
                <div id="logocontainer" class="container">
                    <div id="top-logo-menu-wrap" class="sixteen columns clearfix">
                        <div class="three columns" id="top-logo-wrap">

                            <!-- MAIN LOGO -->

                            <a href="./"><img src="<?php echo base_url() ?>assets/images/main-logo.png" alt="Hotel Express Arrey" id="main-logo" />
                                <img src="<?php echo base_url() ?>assets/images/main-logo-small.jpg" alt="Hotel Express Arrey" id="main-logo-min" /></a>

                        </div>

                        <div class="thirteen columns" id="top-navigation-menu-wrap">

                            <!-- BEGIN MAIN MOBILE NAVIGATION -->
                            <ul id="mobile-navigation-menu">	
                                <li><span class="icon-reorder"></span>
                                    <ul id="mobile-navigation-menu-list">
                                        <li><a href="./">Inicio</a></li>
                                        <li><a href="sobre-o-hotel-express">Sobre</a></li>
                                        <li><a href="apartamento">Reservas</a></li>
                                        <li><a href="http://arreyhotels.com/">
                                        <div class="top-navigation-content-wrap">Arrey Hotels <span class="icon-angle-down"></span><span class="under-title">Conheça a rede de hotéis</span></div></a>
								<ul class="sub_menu">
									<li class="submenu-arrow-wrap"><div class="top-submenu-arrow"></div></li>
									<li><a href="http://www.granhotelarrey.com.br/">Gran Hotel</a></li>
									<li><a href="http://www.formulaflateuropa.com.br/">Fórmula Flat</a></li>
									<li><a href="http://www.executiveflat.com.br/">Executive Flat</a></li>
									<li><a href="http://www.formulaflatitaueira.com.br/">Fórmula Flat Itaueira</a></li>
                                                                        <li><a href="http://www.vivedespacio.com/palaciocarvajalgiron/pt/">Palacio Carvajal Giron</a></li>
								</ul>
							</li>
                                        <!--<li><a href="fotos">Fotos</a></li>-->
                                        <!--<li><a href="localizacao">Localizacao</a></li>-->
                                        <li><a href="contato">Contato</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <!-- END MAIN MOBILE NAVIGATION -->

                            <!-- BEGIN MAIN NAVIGATION -->
                            <ul id="top-navigation-menu" class="dropdown">
                                <li><a href="./">                    <div class="top-navigation-content-wrap">Inicio</div></a></li>
                                <li><a href="sobre-o-hotel-express"> <div class="top-navigation-content-wrap">Sobre</div></a></li>
                                <li><a href="apartamento">           <div class="top-navigation-content-wrap">Apartamentos</div></a></li>
                                <li><a href="http://arreyhotels.com/">
                                        <div class="top-navigation-content-wrap">Arrey Hotels <span class="icon-angle-down"></span><span class="under-title">Conheça a rede de hotéis</span></div></a>
								<ul class="sub_menu">
									<li class="submenu-arrow-wrap"><div class="top-submenu-arrow"></div></li>
									<li><a href="http://www.granhotelarrey.com.br/">Gran Hotel</a></li>
									<li><a href="http://www.formulaflateuropa.com.br/">Fórmula Flat</a></li>
									<li><a href="http://www.executiveflat.com.br/">Executive Flat</a></li>
									<li><a href="http://www.formulaflatitaueira.com.br/">Fórmula Flat Itaueira</a></li>
                                                                        <li><a href="http://www.vivedespacio.com/palaciocarvajalgiron/pt/">Palacio Carvajal Giron</a></li>
								</ul>
							</li>
                                <!--                                <li><a href="fotos">                 <div class="top-navigation-content-wrap">Fotos</div></a></li>-->
                                <!--<li><a href="localizacao">           <div class="top-navigation-content-wrap">Localização</div></a></li>-->
                                <li><a href="contato">               <div class="top-navigation-content-wrap">Contato</div></a></li>
                            </ul>
                            <!-- END MAIN NAVIGATION -->

                        </div>
                    </div>
                </div>
                <!-- END LOGO AND NAVIGATION WRAP -->
                
<!--                 <div id="headcontainer2">
                    <div id="top-sticky-bar" class="container">
                        <div id="sticky-top-bar">
                            <div id="top-contact-wrap">
                                 BEGIN TOP CONTACT INFO 
                                <div id="top-street-address">O Hotel Express Arrey foi inaugurado. Venha conhecer nossas instalações.</div>
                            </div>
                        </div>
                    </div>
                </div> -->

            </header>
            <!-- END HEADER -->
