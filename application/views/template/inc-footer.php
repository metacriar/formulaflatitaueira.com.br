<!-- BEGIN FOOTER -->
<footer>
    <div id="footer-wrap">
        <div class="container">
            <div id="prefooter-wrap">

                <!-- BEGIN FOOTER CONTACT INFO -->
                <div class="five columns">
                    <div class="footer-header">Newsletter</div>
                    <div id="footer-subscribe-text">Assine e fique por dentro de promoções, notícias e muita informação.</div>
                    <input type="text" id="email-newsletter" name="email-newsletter" placeholder="email@dominio.com.br">
                    <button id="footer-subscribe-button">Cadastrar</button>              
                </div>
                <!-- END FOOTER CONTACT INFO -->


                <!-- BEGIN FOOTER CONTACT INFO -->
                <div class="five columns">
                    <div id="footer-contact-info-wrap">
                         <div class="footer-header">Informações</div>
                        <div id="footer-email-wrap">
                            <span id="email-title"><span class="icon-envelope-alt"></span>Email:</span> <span id="email-value">reservas@executiveflat.com.br</span>
                            <div style="clear:both;"></div>
                        </div>
                        <div id="footer-phone-wrap">
                            <span id="phone-title"><span class="icon-phone"></span>Fone:</span> <span id="phone-value">+55(86)3216-6700</span>
                            <div style="clear:both;"></div>
                        </div>
                        <!--                        <div id="footer-skype-wrap">
                                                    <span id="skype-title"><span class="icon-skype"></span>Skype:</span> <span id="skype-value">hotelexpress</span>
                                                    <div style="clear:both;"></div>
                                                </div>-->
                        <div id="footer-address-wrap">
                            <span id="address-title"><span class="icon-compass"></span>Endereço:</span> <span id="address-value"> Rua Regeneração N° 469, Ilhotas - Teresina/PI - Brasil</span>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
                <!-- END FOOTER CONTACT INFO -->

                <!-- BEGIN FOOTER CONNECT WITH US -->
<!--                <div class="five columns offset-by-one">
                    <div class="footer-header">Conecte-se</div>
                    <ul id="flickr-feed">
                        <li>
                            <a href="<?php // echo base_url() ?>assets/images/10323758054_7dd95ff41a_s.jpg">
                                <img src="<?php // echo base_url() ?>assets/images/10323758054_7dd95ff41a_s.jpg" alt="#Envato CEO Collis in his office">
                            </a>
                        </li>
                        <li>
                            <a href="<?php //echo base_url() ?>assets/images/10323758134_cc959a0281_s.jpg">
                                <img src="<?php //echo base_url() ?>assets/images/10323758134_cc959a0281_s.jpg" alt="Inside Envato">
                            </a>
                        </li>
                        <li>
                            <a href="<?php //echo base_url() ?>assets/images/10323758724_9f3f485c96_s.jpg">
                                <img src="<?php //cho base_url() ?>assets/images/10323758724_9f3f485c96_s.jpg" alt="Envato Dev Walkabout">
                            </a>
                        </li>
                        <li>
                            <a href="<?php //echo base_url() ?>assets/images/10323780055_3bcbfddd11_s.jpg">
                                <img src="<?php //echo base_url() ?>assets/images/10323780055_3bcbfddd11_s.jpg" alt="The Pool Competition at Envato">
                            </a>
                        </li>
                        <li>
                            <a href="<?php //echo base_url() ?>assets/images/10323780325_71d9b6dfd1_s.jpg">
                                <img src="<?php //echo base_url() ?>assets/images/10323780325_71d9b6dfd1_s.jpg" alt="Orien ate the wrong lunch!">
                            </a>
                        </li>
                        <li>
                            <a href="<?php //echo base_url() ?>assets/images/10323936643_e387757f47_s.jpg">
                                <img src="<?php //echo base_url() ?>assets/images/10323936643_e387757f47_s.jpg" alt="Check in at Collis' office!">
                            </a>
                        </li>
                        <li>
                            <a href="<?php //echo base_url() ?>assets/images/10323780055_3bcbfddd11_s.jpg">
                                <img src="<?php //echo base_url() ?>assets/images/10323780055_3bcbfddd11_s.jpg" alt="The Pool Competition at Envato">
                            </a>
                        </li>
                        <li>
                            <a href="<?php //echo base_url() ?>assets/images/10323758054_7dd95ff41a_s.jpg">
                                <img src="<?php //echo base_url() ?>assets/images/10323758054_7dd95ff41a_s.jpg" alt="#Envato CEO Collis in his office">
                            </a>
                        </li>
                    </ul>
                    <div style="clear:both"></div>-->
                    <div class="five columns">
                        <div class="footer-header">Siga</div>
                        <div id="footer-social-wrap">
                            <span class="icon-facebook"></span>
                            <span class="icon-twitter"></span>
                            <span class="icon-google-plus"></span>
                            <span class="icon-instagram"></span>
                        </div>
                    </div>
                </div>
                <!-- END FOOTER CONNECT WITH US -->

                <div style="clear:both"></div>

                <!-- BEFIN COPYRIGHT INFO -->
                <div id="copyright-wrap">
                    <div id="copyright-text">Copyright &copy; 2014 Executive Flat. Todos os direitos reservados</div>
                    <div id="copyright-links"><a href="http://www.metacriar.com.br">Desenvolvido pela METACRIAR</a></div>
                </div>
                <!-- END COPYRIGHT INFO -->

            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER -->

</div>
<a href="#to-top" id="back-to-top"><span class="icon-chevron-up"></span></a>

<!-- jQuery LIBRARY -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.js"></script>

<!-- jQuery SMOTH SCROLL  -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/smoothScroll.js"></script>

<!-- jQuery BOOKING CALENDAR PRO -->
<script type="text/javascript" src="<?php //echo base_url() ?>assets/js/jquery.bookingCalendarPRO.js"></script>

<!-- jQuery REVOLUTION SLIDER  -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.themepunch.revolution.min.js"></script>

<!-- jQuery LIGHTBOX -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/lightbox.js"></script>

<!-- MASK FORMS -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/maskedconf.js"></script>

<!-- NATION THEME MAIN SCRIPTS -->
<script src="<?php echo base_url() ?>assets/js/allscript.js"></script>

<!-- DATEPICKER -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>


</body>
</html>
