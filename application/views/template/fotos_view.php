<!-- BEGIN PAGE TITLE -->
<div id="top-content-divider">
    <div id="main-title-wrap">Galeria de Fotos</div>
    <!--<div id="main-title-divider"></div>
    <div id="main-title-undertext">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>-->
</div>
<!-- END PAGE TITLE -->

<!-- BEGIN PAGE CONTENT -->
<div class="container gallery-wrap" id="is-gallery">

    <div class="gallery-item-wrap">
        <div class="gallery-image-wrap">
            <a href="<?php echo base_url() ?>assets/images/gallery1-fullwidth.jpg" style="position:relative;">
                <div class="gallery-overlay-wrap">
                    <img src="<?php echo base_url() ?>assets/images/gallery1.jpg" class="gallery-image">
                    <div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                            <div class="room-overlay-readmore"><span class="icon-search"></span></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="gallery-content-wrap">
            <div class="gallery-header">Room with one bedroom</div>
        </div>
    </div>

    <div class="gallery-item-wrap">
        <div class="gallery-image-wrap">
            <a href="<?php echo base_url() ?>assets/images/gallery2-fullwidth.jpg">
                <div class="gallery-overlay-wrap">
                    <img src="<?php echo base_url() ?>assets/images/room2.jpg" class="gallery-image">
                    <div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                            <div class="room-overlay-readmore"><span class="icon-search"></span></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="gallery-content-wrap">
            <div class="gallery-header">Family Spacious Room</div>
        </div>
    </div>

    <div class="gallery-item-wrap gallery-three-last">
        <div class="gallery-image-wrap">
            <a href="<?php echo base_url() ?>assets/images/gallery3-fullwidth.jpg">
                <div class="gallery-overlay-wrap">
                    <img src="<?php echo base_url() ?>assets/images/room3.jpg" class="gallery-image">
                    <div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                            <div class="room-overlay-readmore"><span class="icon-search"></span></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="gallery-content-wrap">
            <div class="gallery-header">2 Rooms Appartment</div>
        </div>
    </div>

    <div class="gallery-item-wrap">
        <div class="gallery-image-wrap">
            <a href="<?php echo base_url() ?>assets/images/gallery4-fullwidth.jpg">
                <div class="gallery-overlay-wrap">
                    <img src="<?php echo base_url() ?>assets/images/room4.jpg" class="gallery-image">
                    <div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                            <div class="room-overlay-readmore"><span class="icon-search"></span></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="gallery-content-wrap">
            <div class="gallery-header">Standard Double Room</div>
        </div>
    </div>

    <div class="gallery-item-wrap">
        <div class="gallery-image-wrap">
            <a href="<?php echo base_url() ?>assets/images/gallery5-fullwidth.jpg">
                <div class="gallery-overlay-wrap">
                    <img src="<?php echo base_url() ?>assets/images/room5.jpg" class="gallery-image">
                    <div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                            <div class="room-overlay-readmore"><span class="icon-search"></span></div>
                        </div>
                    </div>
                </div>
            </a>	
        </div>
        <div class="gallery-content-wrap">
            <div class="gallery-header">3 Room Appartment</div>
        </div>
    </div>

    <div class="gallery-item-wrap gallery-three-last">
        <div class="gallery-image-wrap">
            <a href="<?php echo base_url() ?>assets/images/gallery6-fullwidth.jpg">
                <div class="gallery-overlay-wrap">
                    <img src="<?php echo base_url() ?>assets/images/room6.jpg" class="gallery-image">
                    <div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                            <div class="room-overlay-readmore"><span class="icon-search"></span></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="gallery-content-wrap">
            <div class="gallery-header">Large business suite</div>	
        </div>
    </div>

    <div class="gallery-item-wrap">
        <div class="gallery-image-wrap">
            <a href="<?php echo base_url() ?>assets/images/gallery7-fullwidth.jpg">
                <div class="gallery-overlay-wrap">
                    <img src="<?php echo base_url() ?>assets/images/room7.jpg" class="gallery-image">
                    <div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                            <div class="room-overlay-readmore"><span class="icon-search"></span></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="gallery-content-wrap">
            <div class="gallery-header">Room with city view</div>
        </div>
    </div>

    <div class="gallery-item-wrap">
        <div class="gallery-image-wrap">
            <a href="<?php echo base_url() ?>assets/images/gallery1-fullwidth.jpg">
                <div class="gallery-overlay-wrap">
                    <img src="<?php echo base_url() ?>assets/images/gallery1.jpg" class="gallery-image">
                    <div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                            <div class="room-overlay-readmore"><span class="icon-search"></span></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="gallery-content-wrap">
            <div class="gallery-header">Standard King Room</div>	
        </div>
    </div>

    <div class="gallery-item-wrap gallery-three-last">
        <div class="gallery-image-wrap">
            <a href="<?php echo base_url() ?>assets/images/gallery4-fullwidth.jpg">
                <div class="gallery-overlay-wrap">
                    <img src="<?php echo base_url() ?>assets/images/room4.jpg" class="gallery-image">
                    <div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                            <div class="room-overlay-readmore"><span class="icon-search"></span></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="gallery-content-wrap">
            <div class="gallery-header">Business Suite</div>	
        </div>
    </div>

</div>
<!-- END PAGE CONTENT -->