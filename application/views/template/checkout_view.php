<?php

if ($_GET["datepickerin"] == "" || $_GET["datepickerout"] == ""){
    header("Location: ./apartamento?msg");
    exit;
} 


$data_01 = $_GET["datepickerin"];
$data_02 = $_GET["datepickerout"];
$data_hoje = date("d/m/Y");
 
// criar os objetos DateTime
// baseados na data brasileira
$obj_data_01 = DateTime::createFromFormat('d/m/Y', $data_01);
$obj_data_02 = DateTime::createFromFormat('d/m/Y', $data_02);
$obj_data    = DateTime::createFromFormat('d/m/Y', $data_hoje);
 
// compara os dois objetos
// qual será o resultado?
if ($obj_data_01 < $obj_data) {
    //menor
   header("Location: ./apartamento?msgckeckin");
   exit;
}

if ($obj_data_02 <= $obj_data_01) {
    //menor
   header("Location: ./apartamento?msgckeckuot");
   exit;
}



?>



<div id="reservation-breadcrumb-wrap" class="container">
    <div >1. Escolha da Data<span class="icon-angle-right"></span></div>
    <div >2. Apartamento <span class="icon-angle-right"></span></div>
    <div id='active'>3. Checkout <span class="icon-angle-right"></span></div>
    <div >4. Confirmação</div>
</div>


<!-- BEGIN PAGE CONTENT -->
<div class="container reservation-page-wrap">
    <div class="five columns">
        <div id="reservation-info" class="step2-reservation-info">						
            <div id="reservation-info-header">Sua Reserva</div>
            <div id="reservation-info-content">
                <div id="reservation-check-in">
                    <div>Check-in</div>
                    <div class="reservation-date-value"><?php echo $_GET["datepickerin"] ?></div>
                </div>
                <div id="reservation-check-out">
                    <div>Check-out</div>
                    <div class="reservation-date-value"><?php echo $_GET["datepickerout"] ?></div>
                </div>
                <div class="clear"></div>
                <div id="reservation-guests">
                    <div>Hóspedes</div>
                    <div id="room-rent-number" style="display:none">1</div>
                    <div class="room-guests-wrap" id="room-guests-wrap1">
                        Apartamentos: <?php echo $_GET["qtdApart"] ?>  | Adultos: <?php echo $_GET["qtdAdulto"] ?>  <br>Crianças: <?php echo $_GET["qtdcrianca"] ?> 
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <div id="roow-par-wrap">
       <div class="container">
			<div class="eleven columns main-reservation-form">
				
				<!-- BEGIN RESERVATION FORM -->
                                <form action="confirmacao/sendMail" method="post">

					<div id="general-info-wrap">
                                            
                                                                          
                                            
						<!-- BEGIN PERSONAL INFO -->
                                                <div id="form-header-divider" style="margin-top: 0">Preencha o formulário abaixo:</div>
						<div id="data-wrap">
							<div class="name-wrap">
								<div class="main-reservation-form-caption">Nome <span class="main-reservation-form-asterisk">*</span></div>
                                                                <input type="text" required="required" name="nome" id="name-field">
							</div>
							<div class="name-wrap surname-wrap">
								<div class="main-reservation-form-caption">Sobrenome <span class="main-reservation-form-asterisk">*</span></div>
                                                                <input type="text" required="required" name="sobrenome" id="surname-field">
							</div>
                                                        <div class="contact-wrap">
								<div class="main-reservation-form-caption">CPF<span class="main-reservation-form-asterisk">*</span></div>
                                                                <input type="text" name="cpf" id="cpf">
							</div>
							<div class="contact-wrap phone-wrap">
								<div class="main-reservation-form-caption">RG <span class="main-reservation-form-asterisk">*</span></div>
								<input type="text" name="rg" required="required" id="phone-field">
							</div>
							<div class="contact-wrap">
								<div class="main-reservation-form-caption">Email <span class="main-reservation-form-asterisk">*</span></div>
								<input type="text" name="email" required="required" id="email-field">
							</div>
							<div class="contact-wrap phone-wrap">
								<div class="main-reservation-form-caption">Telefone <span class="main-reservation-form-asterisk">*</span></div>
                                                                <input type="text" name="fone" id="fone" required="required" id="phone-field">
							</div>
                                                        <div class="main-reservation-form-caption">Observações</div>
                                                        <textarea id="additional-request-text" name="obs"></textarea>
                                                        <input type="hidden" name="datepickerin" value="<?php echo $_GET["datepickerin"]; ?>">
                                                        <input type="hidden" name="datepickerout" value="<?php echo $_GET["datepickerout"]; ?>">
                                                        <input type="hidden" name="qtdApart" value="<?php echo $_GET["qtdApart"]; ?>">
                                                        <input type="hidden" name="qtdAdulto" value="<?php echo $_GET["qtdAdulto"]; ?>">
                                                        <input type="hidden" name="qtdcrianca" value="<?php echo $_GET["qtdcrianca"]; ?>">
                                             
          
						         <button type="submit" onClick="faz()" class="room-reservation-select" style="margin-top: 90px;">Solicitar Reserva</button>
							
						</div>
						<!-- END PERSONAL INFO -->
						
						<!-- BEGIN PAYMENT INFO -->
						
<!--						<div id="data-wrap">
							<div class="main-reservation-form-caption">Numero do cartão de Credito<span class="main-reservation-form-asterisk">*</span></div>
                                                        <input type="text" name="cartao"  value="XXXX-XXXX-XXXX-XXXX" id="credit-card-field">
							<div class="clear:both"></div>
							<div class="credit-card-info-wrap">
								<div class="main-reservation-form-caption">Validade<span class="main-reservation-form-asterisk">*</span></div>
								<input type="text" name="validade" id="card-holder-field">
							</div>
							<div class="credit-card-info-wrap cvv2-code-wrap">
								<div class="main-reservation-form-caption">Codigo de Segurança <span class="main-reservation-form-asterisk">*</span></div>
								<input type="text" name="codSeguranca" id="cvv2-field">
							</div>
							<div class="main-reservation-form-caption">Observações</div>
                                                        <textarea id="additional-request-text" name="obs"></textarea>
                                                        <input type="hidden" name="datepickerin" value="<?php // echo $_GET["datepickerin"]; ?>">
                                                        <input type="hidden" name="datepickerout" value="<?php// echo $_GET["datepickerout"]; ?>">
                                                        <input type="hidden" name="qtdApart" value="<?php //echo $_GET["qtdApart"]; ?>">
                                                        <input type="hidden" name="qtdAdulto" value="<?php //echo $_GET["qtdAdulto"]; ?>">
                                                        <input type="hidden" name="qtdcrianca" value="<?php //echo $_GET["qtdcrianca"]; ?>">
                                                        <input type="hidden" name="totalDiarias" value="<?php //echo $totalDiarias; ?>">
                                                        <input type="hidden" name="precoFinal" value="<?php //echo $precoFinal; ?>">
          
						         <button type="submit" class="room-reservation-select">Confirmar Reserva</button>
						</div>
						 END PAYMENT INFO -->
						
					</div>
				</form>
				<!-- END RESERVATION FORM -->
				
			</div>
			
		</div>
		<!-- END PAGE CONTENT -->
        
        <div style="clear:both"></div>
    </div>
</div>    
