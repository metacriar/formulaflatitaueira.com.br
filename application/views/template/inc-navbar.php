<!-- BEGIN LOGO AND NAVIGATION WRAP -->
<div id="logocontainer" class="container">
    <div id="top-logo-menu-wrap" class="sixteen columns clearfix">
        <div class="three columns" id="top-logo-wrap">

            <!-- MAIN LOGO -->
            <a href="index.html"><img src=""  id="main-logo" /><img src="images/main-logo-small.png" id="main-logo-min" /></a>

        </div>

        <div class="thirteen columns" id="top-navigation-menu-wrap">

            <!-- BEGIN MAIN MOBILE NAVIGATION -->
            <ul id="mobile-navigation-menu">	
                <li><span class="icon-reorder"></span>
                    <ul id="mobile-navigation-menu-list">
                        <li><a href="./">Home</a></li>
                        <li><a href="apartamento">Apartamentos</a></li>
                        <li><a href="galeria"></a></li>
                        <li><a href="contato">Contato</a></li>
                    </ul>
                </li>
            </ul>
            <!-- END MAIN MOBILE NAVIGATION -->

            <!-- BEGIN MAIN NAVIGATION -->
            <ul id="top-navigation-menu" class="dropdown">
                <li><a href="./"><div class="top-navigation-content-wrap">Home <span class="under-title">Pahina Principal</span></div></a></li>
                <li><a href="apartamento"><div class="top-navigation-content-wrap">Apartamentos<span class="under-title">Faça sua reserva online</span></div></a>
                    <!--<ul class="sub_menu">
                            <li class="submenu-arrow-wrap"><div class="top-submenu-arrow"></div></li>
                            <li><a href="rooms-list.html">Rooms 3 Columns</a></li>
                            <li><a href="rooms-list2.html">Rooms 2 Columns</a></li>
                            <li><a href="rooms-list3.html">Rooms 1 Column</a></li>
                            <li><a href="reservation.html">Rooms Single</a></li>
                            <li><a href="backend/test.php">Rooms Admin</a></li>
                            <li><a href="reservation-form.html">Reservation Form</a></li>
                    </ul>-->
                </li>
                <li><a href="fotos"><div class="top-navigation-content-wrap">Fotos<span class="under-title">A melhor estrutura</span></div></a></li>
                <!--<li><a href="contact.html"><div class="top-navigation-content-wrap">Localização<span class="under-title">Fácil para encontrar</span></div></a></li>-->
                <li><a href="contato"><div class="top-navigation-content-wrap">Contato</div></a></li>
            </ul>
            <!-- END MAIN NAVIGATION -->

        </div>
    </div>
</div>
<!-- END LOGO AND NAVIGATION WRAP -->