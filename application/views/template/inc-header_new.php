<?php 

    $pageHeader = new PageHeader();
    
    $pageHeader->addMeta('viewport', 'width=device-width, initial-scale=1');
    $pageHeader->addMeta('author', 'Huggo DeLima');
    $pageHeader->addKeyword('Ralny, Rose');
    $pageHeader->addKeyword('Pablo');
    
    $pageHeader->addLibraryFile("bootstrap/css", "bootstrap", "css");
    $pageHeader->addLibraryFile("bootstrap/css", "bootstrap-responsive", "css");
    $pageHeader->addLibraryFile("bootstrap/js", "jquery", "js", '');
    $pageHeader->addLibraryFile("bootstrap/js", "bootstrap.min", "js", '');
    $pageHeader->addLibraryFile("jQuery", "jquery-1.4.2", "js", '');
    
    $pageHeader->write();

?>