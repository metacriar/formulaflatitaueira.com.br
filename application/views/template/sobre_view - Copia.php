<!-- BEGIN PAGE TITLE -->
<div id="top-content-divider">
    <div id="main-title-wrap">SOBRE O HOTEL EXPRESS</div>
    <div id="main-title-undertext">A sua localização privilegiada faz com que seja a melhor opção de hospedagem para viagens de negócios ou visitas à cidade de Teresina. </div>
</div>
<!-- END PAGE TITLE -->

<!-- BEGIN PAGE CONTENT -->
<div class="container">
    <div class="eleven columns page-wrap">
        <h2>Curabitur ornare iaculis erat imperdiet</h2>
        <p>Etiam luctus malesuada quam eu aliquet. Donec eget mollis tortor. Donec pellentesque eros a nisl euismod, ut congue orci ultricies. Fusce aliquet metus non arcu varius ullamcorper a sit amet nunc. Donec in lacus neque. Vivamus ullamcorper sed ligula vitae dapibus. Proin non dolor ornare, laoreet tortor non, congue enim. Mauris vitae feugiat ante. In rhoncus felis eget risus vehicula interdum. Vestibulum tempus diam lectus, et suscipit mauris varius nec. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent vitae arcu semper, tempor est non, convallis ligula. Suspendisse potenti. Maecenas sit amet ornare tortor. Aliquam adipiscing pretium lobortis.</p>
        <blockquote>Ut velit sem, congue at egestas eu, elementum nec mi. Vivamus venenatis cursus nulla, non fringilla orci congue ut. Nam id diam volutpat, dictum risus vel, elementum justo. Sed urna nisi, ultricies non posuere non.</blockquote>
        <p>Aliquam venenatis leo a accumsan posuere. Nulla sodales pellentesque lorem pharetra lacinia. Nullam ut nisi eleifend, vehicula erat eu, hendrerit felis. Etiam molestie placerat elit, ut dignissim lorem aliquet non. Nunc scelerisque risus enim, eu venenatis turpis interdum at. Sed vitae placerat felis. Nunc felis est, lobortis ac luctus sit amet, condimentum eu orci. In eu laoreet dui.</p>	
        <img src="<?php echo base_url() ?>assets/images/room4.jpg" class="left-align-image">
        <p>Mauris risus diam, malesuada eu libero eget, eleifend ultrices lorem. Etiam aliquet elit sed nisl porttitor rutrum. Nulla lectus nibh, rutrum quis vulputate ac, iaculis lobortis mauris. Maecenas ultricies nibh a venenatis adipiscing. Sed libero tortor, adipiscing luctus pellentesque eget, consequat a nibh. Suspendisse malesuada mauris sed eleifend consequat. Nulla non nunc dui. Quisque eu porttitor nibh, at tempor velit. Sed sed sem dui. Phasellus vehicula scelerisque sodales. Sed luctus urna elit, id rutrum felis adipiscing ac. Proin in mauris ac tortor fringilla porttitor. Proin rutrum suscipit lectus, id hendrerit neque viverra id. Curabitur rutrum dignissim tempus. Vestibulum feugiat laoreet consectetur. Suspendisse interdum, urna a ultrices placerat, augue dui molestie massa, id pulvinar lectus ante non enim. Donec ac pellentesque massa. Donec porttitor aliquam vestibulum. Cras pellentesque commodo elit vel fringilla. Donec sit amet magna nec libero luctus feugiat sed non nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. In laoreet lobortis feugiat. </p>
        <img src="<?php echo base_url() ?>assets/images/room1.jpg" class="right-align-image">
        <p>Mauris risus diam, malesuada eu libero eget, eleifend ultrices lorem. Etiam aliquet elit sed nisl porttitor rutrum. Nulla lectus nibh, rutrum quis vulputate ac, iaculis lobortis mauris. Maecenas ultricies nibh a venenatis adipiscing. Sed libero tortor, adipiscing luctus pellentesque eget, consequat a nibh. Suspendisse malesuada mauris sed eleifend consequat. Nulla non nunc dui. Quisque eu porttitor nibh, at tempor velit. Sed sed sem dui. Phasellus vehicula scelerisque sodales. Sed luctus urna elit, id rutrum felis adipiscing ac. Proin in mauris ac tortor fringilla porttitor. Proin rutrum suscipit lectus, id hendrerit neque viverra id. Curabitur rutrum dignissim tempus. Vestibulum feugiat laoreet consectetur. Suspendisse interdum, urna a ultrices placerat, augue dui molestie massa, id pulvinar lectus ante non enim. Donec ac pellentesque massa. Donec porttitor aliquam vestibulum. Cras pellentesque commodo elit vel fringilla. Donec sit amet magna nec libero luctus feugiat sed non nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. In laoreet lobortis feugiat. Mauris risus diam, malesuada eu libero eget, eleifend ultrices lorem. Etiam aliquet elit sed nisl porttitor rutrum. Nulla lectus nibh, rutrum quis vulputate ac, iaculis lobortis mauris. Maecenas ultricies nibh a venenatis adipiscing. Sed libero tortor, adipiscing luctus pellentesque eget, consequat a nibh. Suspendisse malesuada mauris sed eleifend consequat.</p>
    </div>

    <!-- BEGIN PAGE SIDEBAR -->
    <div class="four columns offset-by-one page-sidebar">

        <!-- SIDEBAR SEARCH -->
        <!--<input type="text" value="Search ..." id="sidebar-search">-->

        <!-- BEGIN SIDEBAR POPULAR POSTS -->
     
        <div class="sidebar-header">Notícias</div>
        <div id="popular-posts-wrap">
            <div class="popular-post-wrap">
                <img src="<?php echo base_url() ?>assets/images/popular-post3.jpg" class="popular-post-image">
                <div class="popular-post-content-wrap">
                    <div class="popular-post-header">Lorem Ipsum Sit Doler</div>
                    <div class="popular-post-meta">20 Out, 2014 by Ralny Andrade</div>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="popular-post-wrap">
                <img src="<?php echo base_url() ?>assets/images/popular-post1.jpg" class="popular-post-image">
                <div class="popular-post-content-wrap">
                    <div class="popular-post-header">Lorem Ipsum Sit Doler</div>
                    <div class="popular-post-meta">20 Out, 2014 by Ralny Andrade</div>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="popular-post-wrap">
                <img src="<?php echo base_url() ?>assets/images/popular-post5.jpg" class="popular-post-image">
                <div class="popular-post-content-wrap">
                    <div class="popular-post-header">Lorem Ipsum Sit Doler</div>
                    <div class="popular-post-meta">20 Out, 2014 by Ralny Andrade</div>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>			
      
        <!-- END SIDEBAR POPULAR POSTS -->

        <!-- BEGIN SIDEBAR UPCOMMING EVENTS -->
        <!--
        <div class="sidebar-header">Upcomming Events</div>
        <div class="sidebar-events-wrap">
            <img src="images/event-sidebar1.jpg" class="popular-post-image">
            <div class="sidebar-events-header">
                2014 New Year Celebrations!
            </div>
            <div class="sidebar-events-meta">
                31 Dec, 2013 - 5 Jan, 2014
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="sidebar-events-wrap">
            <img src="images/event-sidebar2.jpg" class="popular-post-image">
            <div class="sidebar-events-header">
                2013 Christmas Celebrations!
            </div>
            <div class="sidebar-events-meta">
                25 Dec, 2013 - 29 Dec, 2013
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="sidebar-events-wrap">
            <img src="images/event-sidebar3.jpg" class="popular-post-image">
            <div class="sidebar-events-header">
                Nation Hotel Birthday!
            </div>
            <div class="sidebar-events-meta">
                10 Oct, 2013 - 13 Oct, 2013
            </div>
            <div style="clear:both"></div>
        </div>
        -->
        <!-- END SIDEBAR UPCOMMING EVENTS -->

       <!-- BEGIN SIDEBAR CONTACT INFO -->
        <div class="sidebar-header">Para mais informações</div>
        <div id="contact-us-wrap">			
            <div id="contact-us-wrap-intro">Você esta com dúvidas ou com problemas com a reservas on-line? Entre em contato com um de nossos canais de atendimento.</div>
            <ul>
                <li id="by-phone">
                    <span class="icon-mobile-phone"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">Telefone</div> <br />(86) 3214-9292</div>
                    <div style="clear:both"></div>
                </li>
                <li id="by-email">
                    <span class="icon-envelope-alt"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">email:</div> <br />reservas@hotelexpressarrey.com.br</div>
                    <div style="clear:both"></div>
                </li>
                <li id="by-skype">
                    <span class="icon-skype"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">skype:</div> <br />hotelexpressarrey</div>
                    <div style="clear:both"></div>
                </li>
            </ul>
        </div>	
        <!-- END SIDEBAR CONTACT INFO -->

    </div>
    <!-- END PAGE SIDEBAR -->

</div>
<!-- END PAGE CONTENT -->