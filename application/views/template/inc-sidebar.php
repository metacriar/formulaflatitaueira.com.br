

<!-- Page container -->
<div class="page-container">
    <!-- Sidebar-->
    <div class="sidebar">
        <div class="sidebar-content">
            <!-- User dropdown -->
            <div class="user-menu dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url() ?>assets/images/demo/users/face3.png" alt="">
                    <div class="user-info">Madison Gartner <span>Web Developer</span></div>
                </a>
                <div class="popup dropdown-menu dropdown-menu-right">
                    <div class="thumbnail">
                        <div class="thumb"><img alt="" src="<?php echo base_url() ?>assets/images/demo/users/face3.png">
                            <div class="thumb-options"><span><a href="#" class="btn btn-icon btn-success"><i class="icon-pencil"></i></a><a href="#" class="btn btn-icon btn-success"><i class="icon-remove"></i></a></span></div>
                        </div>
                        <div class="caption text-center">
                            <h6>Madison Gartner <small>Front end developer</small></h6>
                        </div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item"><i class="icon-pencil3 text-muted"></i> My posts <span class="label label-success">289</span></li>
                        <li class="list-group-item"><i class="icon-people text-muted"></i> Users online <span class="label label-danger">892</span></li>
                        <li class="list-group-item"><i class="icon-stats2 text-muted"></i> Reports <span class="label label-primary">92</span></li>
                        <li class="list-group-item"><i class="icon-stack text-muted"></i> Balance
                            <h5 class="pull-right text-danger">$45.389</h5>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /user dropdown -->
            <!-- Main navigation -->
            <ul class="navigation">
                <li class="active"><a href="index.html"><span>Dashboard</span> <i class="icon-screen2"></i></a></li>
                <li><a href="index.html"><span>Agenda</span> <i class="icon-screen2"></i></a></li>
                <li><a href="#" class="expand"><span>Entidades</span> <i class="icon-paragraph-justify2"></i></a>
                    <ul>
                        <li><a href="entidade">Clientes</a></li>
                        <li><a href="entidade">Fornedores</a></li>
                        <li><a href="entidade">Commissionados</a></li>
                        <li><a href="entidade">Colaboradores</a></li>
                    </ul>
                </li>
                <li><a href="index.html"><span>Processos</span> <i class="icon-screen2"></i></a></li>
                <li><a href="index.html"><span>Financeiro</span> <i class="icon-screen2"></i></a></li>
                <li><a href="#" class="expand"><span>Configurações</span> <i class="icon-paragraph-justify2"></i></a>
                    <ul>
                        <li><a href="usuario">Equipe</a></li>
                    </ul>
                </li>
                <li><a href="#" class="expand"><span>Recepção</span> <i class="icon-paragraph-justify2"></i></a>
                    <ul>
                        <li><a href="usuario">Controle de ligações</a></li>
                        <li><a href="usuario">Registro de Ponto</a></li>
                        <li><a href="usuario">Agenda de Contatos</a></li>
                    </ul>
                </li>
                <li><a href="#" class="expand"><span>Gestão de Documentos</span> <i class="icon-paragraph-justify2"></i></a>
                    <ul>
                        <li><a href="usuario">Dashboad</a></li>
                        <li><a href="usuario">Gerador de Documentos</a></li>
                    </ul>
                </li>
                <li><a href="index.html"><span>Protocolo</span> <i class="icon-screen2"></i></a></li>
                <li><a href="index.html"><span>Gestão de Documentos</span> <i class="icon-screen2"></i></a></li>
                <li><a href="index.html"><span>Rotinas</span> <i class="icon-screen2"></i></a></li>

            </ul>
            <!-- /main navigation -->
        </div>
    </div>
    <!-- /sidebar -->