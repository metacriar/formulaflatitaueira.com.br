<!-- BEGIN FRONT PAGE SLIDER -->
<div id="main-slider-wrap">
    <div class="bannercontainer">
        <div class="banner">
            <ul shadow="0">

                <!-- BEGIN FIRST SLIDE -->
                <li data-transition="papercut" data-slotamount="5" data-masterspeed="300" data-slideindex="back">

                    <!-- THE MAIN IMAGE IN THE SLIDE -->
                    <img src="<?php echo base_url() ?>assets/images/slide1.jpg" >

                    <!-- CAPTIONS OF FIRST SLIDE -->
                 
                    <div class="caption new_small_text lfb fadeout"
                            data-x="0"
                            data-y="218"
                            data-speed="500"
                            data-start="1500"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo">
                    </div> 
                    
                    <div class="caption text-intro lfl fadeout normal-text-intro"
                            data-x="20"
                            data-y="255"
                            data-speed="500"
                            data-start="1000"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo">O Hotel Express Arrey foi inaugurado.
                    </div> 
                    
                    <div class="caption large_text lfb fadeout normal-large-text"
                            data-x="20"
                            data-y="330"
                            data-speed="500"
                            data-start="2000"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo">venha conhecer nossas instalações!</div> 
                    
                    <div class="caption text-intro lfl fadeout responsive-text-intro"
                            data-x="90"
                            data-y="255"
                            data-speed="500"
                            data-start="1000"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo">
                    </div> 
                    
                    <div class="caption large_text lfb fadeout responsive-large-text"
                            data-x="90"
                            data-y="330"
                            data-speed="500"
                            data-start="2000"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo"></div> 
            <!--
                     <div class="caption lfr ltb man-image slide-screen"
                            data-x="630"
                            data-y="76"
                            data-speed="1000"
                            data-start="2500"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo"><img src="images/man2.png"> 
                    </div> 
                -->  
                </li>
                <!-- END FIRST SLIDE -->

                <li data-transition="papercut" data-slotamount="5" data-masterspeed="300" data-slideindex="back">

                    <!-- THE MAIN IMAGE IN THE SLIDE -->
                    <img src="<?php echo base_url() ?>assets/images/slide2.jpg" >

                    <!-- CAPTIONS OF FIRST SLIDE -->
                  
                    <div class="caption new_small_text lfb fadeout"
                            data-x="0"
                            data-y="218"
                            data-speed="500"
                            data-start="1500"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo">
                    </div> 
                    
                    <div class="caption text-intro lfl fadeout normal-text-intro"
                            data-x="20"
                            data-y="255"
                            data-speed="500"
                            data-start="1000"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo">HOTEL EXPRESS ARREY
                    </div> 
                    
                    <div class="caption large_text lfb fadeout normal-large-text"
                            data-x="20"
                            data-y="330"
                            data-speed="500"
                            data-start="2000"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo">PRATICIDADE E ECONOMIA SE HOSPEDAM AQUI
                                
                    </div> 
                    
                    <div class="caption text-intro lfl fadeout responsive-text-intro"
                            data-x="90"
                            data-y="255"
                            data-speed="500"
                            data-start="1000"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo">
                    </div> 
                    
                    <div class="caption large_text lfb fadeout responsive-large-text"
                            data-x="90"
                            data-y="330"
                            data-speed="500"
                            data-start="2000"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo"></div> 
            
<!--                     <div class="caption lfr ltb man-image slide-screen"
                            data-x="630"
                            data-y="76"
                            data-speed="1000"
                            data-start="2500"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo"><img src="images/man2.png"> 
                    </div> -->
                   
                </li>
                <!-- END FIRST SLIDE -->

                <!-- BEGIN SECOND SLIDE -->
                <!--<li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="300" data-slideindex="back">
                    <!-- THE MAIN IMAGE IN THE SLIDE --
                    <img src="<?php // echo base_url() ?>assets/images/dsc-45201.jpg.1920x807_default.jpg" >

                    <!-- THE CAPTIONS OF THE FIRST SLIDE -->
                    <!--
                    <div class="caption lfr ltb slide-screen"
                            data-x="630"
                            data-y="50"
                            data-speed="1000"
                            data-start="500"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo"><img src="images/slide-bg2.png"> 
                    </div> 
                    <div class="caption lfr ltb slide-screen"
                            data-x="1055"
                            data-y="235"
                            data-speed="1000"
                            data-start="1000"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo"><img src="images/slide-iphone.png"> 
                    </div> 
                    <div class="caption lfr ltb slide-screen"
                            data-x="550"
                            data-y="210"
                            data-speed="1000"
                            data-start="1500"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo"><img src="images/slide-ipad.png"> 
                    </div> 
                    <div class="caption text-intro lfr fadeout"
                            data-x="0"
                            data-y="240"
                            data-speed="300"
                            data-start="2000"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo">LOOKS GREAT
                    </div> 
                    <div class="caption large_text lfb fadeout"
                            data-x="0"
                            data-y="316"
                            data-speed="300"
                            data-start="2500"
                            data-captionhidden="on"
                            data-endeasing="easeOutExpo"
                            data-easing="easeOutExpo">on any mobile devices you can imagine
                    </div> 
                    

                </li>	
                <!-- END SECOND SLIDE -->

            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>
</div>
<!-- END FRONT PAGE SLIDER -->

<!-- BEGIN MAIN ROOM VIEW -->
<!--
<div id="room-view-wrap" class="main-rooms-list">
        <div class="container">

                <div id="main-news-header">Apartamentos</div>
                <div id="main-news-subheader">Apartamentos novos, inteiramente redecorados e revitalizados. Compostos por salas confortáveis, cozinha, varanda, Internet banda larga, TV a cabo, cofre individual, estações de trabalho e ar-condicionado.</div>	
                
                <div id="room-view-content">
                        
<!-- BEGIN FIRST ROOM WRAP 
<div class="rooms-list-item-wrap">
        <div class="rooms-list-item-image-wrap">
                <img src="<?php // echo base_url()   ?>assets/images/room1.jpg" id="rooms-list-image">
                <a href="reservation.html" class="room-overlay-link"><div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                                <a href="reservation.html"><div class="room-overlay-readmore"><span class="icon-search"></span></div></a>
                                <div class="room-overlay-checkavail overlay-checkavail2" id="room-main-one"><span class="icon-calendar"></span></div>
                        </div>
                        </div></a>
                <div class="rooms-list-item-price">
                        RS 190,00<span></span>
                        <div class="price-shadow"></div>
                </div>
        </div>
        <div class="rooms-list-content-wrap module-bottom">
                <a href="reservation.html"><div class="rooms-list-header">Apartamento Standart</div></a>
                <div class="underheader-line"></div>
                <div class="room-list-parametr">
                        <div id="room-person">Max. pessoas: <span class="icon-male"></span><span class="icon-male"></span><span class="icon-male"></span></div>
                        <div id="room-bed">1 Cama Kingsize, 1 Cama de Soteiro</div>
                </div>				
        </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">							
                <div class="modal-body">
                        <div id="frontend"></div>
                </div>
                <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar <span class="icon-remove"></span></button>
                </div>
        </div>
</div>
<!-- END FIRST ROOM WRAP -->

<!-- BEGIN SECOND ROOM WRAP -
<div class="rooms-list-item-wrap">
        <div class="rooms-list-item-image-wrap">
                <img src="<?php // echo base_url()   ?>assets/images/room2.jpg" id="rooms-list-image">
                <a href="reservation.html" class="room-overlay-link"><div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                                <a href="reservation.html"><div class="room-overlay-readmore"><span class="icon-search"></span></div></a>
                                <div class="room-overlay-checkavail overlay-checkavail" id="room-main-one"><span class="icon-calendar"></span></div>
                        </div>
                </div></a>
                <div class="rooms-list-item-price">
                        R$ 210,00<span></span>
                        <div class="price-shadow"></div>
                </div>
        </div>
        <div class="rooms-list-content-wrap module-bottom">
                <a href="reservation.html"><div class="rooms-list-header">Suite Luxo Single</div></a>
                <div class="underheader-line"></div>
                <div class="room-list-parametr">
                        <div id="room-person">Max. pessoas: </span><span class="icon-male"></span><span class="icon-male"></span></div>
                        <div id="room-bed">1 Cama de casa Kingsize</div>
                </div>
        </div>	
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">							
                <div class="modal-body">
                        <div id="frontend2"></div>
                </div>
                <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar <span class="icon-remove"></span></button>
                </div>
        </div>
</div>
<!-- END SECOND ROOM WRAP -->

<!-- BEGIN THIRD ROOM WRAP -
<div class="rooms-list-item-wrap">
        <div class="rooms-list-item-image-wrap">
                <img src="<?php // echo base_url()   ?>assets/images/room3.jpg" id="rooms-list-image">
                <a href="reservation.html" class="room-overlay-link"><div class="room-main-list-overlay">
                        <div class="room-overlay-content">
                                <a href="reservation.html"><div class="room-overlay-readmore"><span class="icon-search"></span></div></a>
                                <div class="room-overlay-checkavail overlay-checkavail3" id="room-main-one"><span class="icon-calendar"></span></div>
                        </div>
                </div></a>
                <div class="rooms-list-item-price">
                        R$ 270,00<span></span>
                        <div class="price-shadow"></div>
                </div>
        </div>
        <div class="rooms-list-content-wrap module-bottom">
                <a href="reservation.html"><div class="rooms-list-header">Suite Luxo Double </div></a>
                <div class="underheader-line"></div>
                <div class="room-list-parametr">
                        <div id="room-person">Max. pessoas: <span class="icon-male"></span><span class="icon-male"></span><span class="icon-male"></span><span class="icon-male"></span></div>
                        <div id="room-bed">1 Cama Kingsize, 2 Camas de solteiro</div>
                </div>					
        </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">							
                <div class="modal-body">
                        <div id="frontend3"></div>
                </div>
                <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar <span class="icon-remove"></span></button>
                </div>
        </div>
</div>
<!-- END THIRD ROOM WRAP

</div>
</div>
</div>
-->
<!-- BEGIN MAIN ROOM VIEW -->

<!-- BEGIN MADE RESERVATION WRAP -->
<div id="reservation-text-wrap">
    <div class="container">
        <div id="main-reservation-text">
            A melhor opção de hospedagem para viagens de negócios ou visitas à cidade de Teresina.
        </div>
    </div>
</div>	
<!-- END MADE RESERVATION WRAP -->		

<!-- BEGIN INFO SECTION -->
<div id="information-wrap" style="margin-top: 50px;">
    <div class="container">

        <!-- BEGIN VISITORS TESTIMONIALS -->
        <div class="six columns module-side module-side-left">
            
            <div class="testimonials-content-wrap">
                <div class="header-wrap">
                <span class="header-text"> 
                    <span class="icon-angle-left"></span> Informações <span class="icon-angle-right"></span>
                </span>
            </div>
                <ul id="general-info">
                <li><span class="icon-time"></span>Check-in a parti das: 12:00 hs</li>
                <li><span class="icon-globe"></span>Wi-Fi gratis em todo o hotel</li>
                <li><span class="icon-food"></span>Café da manhã<br /> <small>(não incluso na diária)</small></li>
                <li><span class="icon-truck"></span>Estacionamento exclusivo para clientes</li>
                <li><span class="icon-shopping-cart"></span>Espaço mall</li>
             

            </ul>
            </div>
            
            
            <div class="testimonials-content-wrap">
                <div class="header-wrap">
                <span class="header-text"> 
                    <span class="icon-angle-left"></span> Localização <span class="icon-angle-right"></span>
                </span>
            </div>
                <div id="gmaps"></div>
            </div>
            
        </div>
        <!-- END VISITORS TESTIMONIALS -->

        <!-- BEGIN ABOUT US SECTION -->
        <div class="nine columns offset-by-one module-side">
            <div class="header-wrap">
                <span class="header-text"> 
                    <span class="icon-angle-left"></span>Sobre o Hotel Express<span class="icon-angle-right"></span>
                </span>
            </div>
            <div id="main-aboutus-wrap">
                <div id="main-aboutus-intro">A sua localização privilegiada faz com que seja a melhor opção de hospedagem para viagens de negócios ou visitas à cidade de Teresina.
                    Próximo a shoppings, boates e diversos restaurantes. Uma variedade de opções para lazer, compras e alimentação esperando por você!
                </div>
                <div id="first-par-wrap">
                    <div class="four columns left-aboutus-column"><img src="<?php echo base_url() ?>assets/images/aboutus1.jpg" width="250px" style="float:left"></div>
                    <div class="five columns right-aboutus-column">
                        O Hotel Express conta com 6 andares e 170 apartamentos luxo com ar-condicionado, TV de LED a cabo, frigobar, acesso a internet grátis e telefone.
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div style="clear:both"></div>
                <div id="second-par-wrap">
                    <div class="five columns left-aboutus-column">
                        Além de espaço mall de serviços na área terrea, trazendo toda comodidade aos hóspedes .
                    </div>
                    <div class="four columns right-aboutus-column"><img src="<?php echo base_url() ?>assets/images/aboutus2.jpg" width="250px" style="float:right"></div>
                    <div style="clear:both"></div>
                </div>
            </div>
        </div>
        <!-- END ABOUT US SECTION -->

    </div>
</div>
<!-- END INFO SECTION -->

<!-- BEGIN LOCATION INFO EVENT WRAP -->
<!--<div id="location-wrap">
    <div class="container">

         BEGIN EVENTS WRAP 
        <div class="five columns module">
            <div class="header-wrap">
                <span class="header-text"> 
                    <span class="icon-angle-left"></span> Pacotes <span class="icon-angle-right"></span>
                </span>
            </div>
            <div class="event-wrap">
                <div class="event-header">Pacote 1</div>
                <div class="event-date">17 de Outubro de 2014</div>
                <div class="event-header-divider"></div>
                <div class="event-offer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce neque mi, fermentum.<br />
                    <a href="blog-single.html"><div class="event-more-button">Ler Mais <span class="icon-eye-open"></span></div></a>
                </div>
            </div>
            <div class="event-wrap">
                <div class="event-header">Pacote 2!</div>
                <div class="event-date">31 de Dezembro de 2014</div>
                <div class="event-header-divider"></div>
                <div class="event-offer">Phasellus dui quam, congue ac risus et, vehicula venenatis sapien. Curabitur at lacus varius.<br />
                    <a href="blog-single.html"><div class="event-more-button">Ler Mais <span class="icon-eye-open"></span></div></a>
                </div>
            </div>					
        </div>
         END EVENTS WRAP 

         BEGIN LOCATIONS WRAP 
        <div class="six columns module">
            <div class="header-wrap">
                <span class="header-text"> 
                    <span class="icon-angle-left"></span> Localização <span class="icon-angle-right"></span>
                </span>
            </div>
            <div id="main-map-text">Clique no mapa abaixo para obter direção: </div>
            <div id="gmaps"></div>
        </div>
         END LOCATIONS WRAP 

         BEGIN INFO WRAP 
        <div class="five columns module">
            <div class="header-wrap">
                <span class="header-text"> 
                    <span class="icon-angle-left"></span> Infomações <span class="icon-angle-right"></span>
                </span>
            </div>
            <ul id="general-info">
                <li><span class="icon-time"></span>Check-in a parti das: 12:00 hs</li>
                <li><span class="icon-globe"></span>Wi-Fi gratis em todo o hotel</li>
                <li><span class="icon-food"></span>Café da manhã</li>
                <li><span class="icon-truck"></span>Estacionamento exclusivo para clientes</li>
                <li><span class="icon-shopping-cart"></span>Espaço mall</li>

            </ul>
        </div>
         END INFO WRAP 

    </div>
</div>-->
<!-- END LOCATION INFO EVENT WRAP -->

<!-- BEGIN NEWS WRAP --
<div id="main-news-wrap" class="container">
        <div class="container">
                
                <div id="main-news-header">Ultimas Novidades</div>
                <div id="main-news-subheader">Para saber mais sobre o que aconteceu recentemente no nosso hotel, por favor, clique em uma das imagens abaixo, ou leia mais botão.</div>
                
                <div id="main-blog-posts-wrap" class="four columns">
                        <div class="main-blog-post-wrap">
                                <div class="main-blog-post-image-wrap">
                                        <img src="<?php // echo base_url()   ?>assets/images/main-news1.jpg" class="main-news-image">
                                        <a href="blog-single.html" class="room-overlay-link"><div class="room-main-list-overlay">
                                                <div class="room-overlay-content">
                                                        <a href="blog-single.html"><div class="room-overlay-readmore"><span class="icon-search"></span></div></a>
                                                </div>
                                        </div></a>
                                </div>
                                <div class="main-blog-post-content-wrap">
                                        <div class="main-blog-post-header">Morbi tincidunt turpis</div>
                                        <div class="event-header-divider"></div>
                                        <div class="main-blog-post-content">Ut tempus in quam in pellentesque. Phasellus hendrerit turpis. </div>
                                </div>
                        </div>
                </div>
        
                <div id="main-blog-posts-wrap" class="four columns">
                        <div class="main-blog-post-wrap">
                                <div class="main-blog-post-image-wrap">
                                        <img src="<?php // echo base_url()   ?>assets/images/main-news2.jpg" class="main-news-image">
                                        <a href="blog-single.html" class="room-overlay-link"><div class="room-main-list-overlay">
                                                <div class="room-overlay-content">
                                                        <a href="blog-single.html"><div class="room-overlay-readmore"><span class="icon-search"></span></div></a>
                                                </div>
                                        </div></a>
                                </div>
                                <div class="main-blog-post-content-wrap">
                                        <div class="main-blog-post-header">Eget placerat convallis </div>
                                        <div class="event-header-divider"></div>
                                        <div class="main-blog-post-content">Ut tempus in quam in pellentesque. Phasellus hendrerit turpis. </div>
                                </div>
                        </div>
                </div>
        
                <div id="main-blog-posts-wrap" class="four columns">
                        <div class="main-blog-post-wrap">
                                <div class="main-blog-post-image-wrap">
                                        <img src="<?php // echo base_url()   ?>assets/images/main-news3.jpg" class="main-news-image">
                                        <a href="blog-single.html" class="room-overlay-link"><div class="room-main-list-overlay">
                                                <div class="room-overlay-content">
                                                        <a href="blog-single.html"><div class="room-overlay-readmore"><span class="icon-search"></span></div></a>
                                                </div>
                                        </div></a>
                                </div>
                                <div class="main-blog-post-content-wrap">
                                        <div class="main-blog-post-header">Donec vel neque</div>
                                        <div class="event-header-divider"></div>
                                        <div class="main-blog-post-content">Ut tempus in quam in pellentesque. Phasellus hendrerit turpis. </div>
                                </div>
                        </div>
                </div>
        
                <div id="main-blog-posts-wrap" class="four columns">
                        <div class="main-blog-post-wrap">
                                <div class="main-blog-post-image-wrap">
                                        <img src="<?php // echo base_url()   ?>assets/images/main-news4.jpg" class="main-news-image">
                                        <a href="blog-single.html" class="room-overlay-link"><div class="room-main-list-overlay">
                                                <div class="room-overlay-content">
                                                        <a href="blog-single.html"><div class="room-overlay-readmore"><span class="icon-search"></span></div></a>
                                                </div>
                                        </div></a>
                                </div>
                                <div class="main-blog-post-content-wrap">
                                        <div class="main-blog-post-header">Cras malesuada pretium</div>
                                        <div class="event-header-divider"></div>
                                        <div class="main-blog-post-content">Ut tempus in quam in pellentesque. Phasellus hendrerit turpis. </div>
                                </div>
                        </div>
                </div>

        </div>
</div>
<!-- END NEWS WRAP -->