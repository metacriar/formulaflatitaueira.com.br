
<div id="reservation-breadcrumb-wrap" class="container">
    <div >1. Escolha da Data<span class="icon-angle-right"></span></div>
    <div id='active'>2. Apartamento <span class="icon-angle-right"></span></div>
    <div >3. Checkout <span class="icon-angle-right"></span></div>
    <div >4. Confirmação</div>
</div>


<!-- BEGIN PAGE CONTENT -->
<div class="container reservation-page-wrap">
    <div class="five columns">
        <div id="reservation-info" class="step2-reservation-info">						
            <div id="reservation-info-header">Sua Reserva</div>
            <div id="reservation-info-content">
                <div id="reservation-check-in">
                    <div>Check-in</div>
                    <div class="reservation-date-value"></div>
                </div>
                <div id="reservation-check-out">
                    <div>Check-out</div>
                    <div class="reservation-date-value"></div>
                </div>
                <div class="clear"></div>
                <div id="reservation-guests">
                    <div>Hóspedes</div>
                    <div id="room-rent-number" style="display:none">1</div>
                    <div class="room-guests-wrap" id="room-guests-wrap1">
                        Apartamentos:   | Adultos:   <br>Crianças:  
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="roow-par-wrap">
        
        <div id="roow-description-wrap" style="font-size: 20px; font-weight: 500; padding-bottom: 10px;">    
            Sua solicitação será efetivada conforme disponibilidade. Aguarde email de confirmação.
        </div>
        <form action="checkout">
        <div class="four columns left-aboutus-column"><img src="http://localhost/projetos/hotelexpress.com.br/assets/images/aboutus1.jpg" width="250px" style="float:left"></div>
        <div class="room-reservation-title">Apartamento Luxo</div>
        <div id="roow-description-wrap">
            Apartamento Luxo, possuem ar-condicionado, banheiro individual, TV a cabo LCD, frigobar, acesso a internet grátis e telefone.
        </div>
        <div class="room-reservation-price">
            <div>
                <span>Tarifa de </span> R$ 250,00<span> / Diária</span>
            </div>
        </div>  
        <div style="clear:both"></div>
        <input type="hidden" name="datepickerin" value="">
        <input type="hidden" name="datepickerout" value="">
        <input type="hidden" name="qtdApart" value="">
        <input type="hidden" name="qtdAdulto" value="">
        <input type="hidden" name="qtdcrianca" value="">
        <button type="submit" class="room-reservation-select">Solicitar Reserva</button>
        </form>
        <div style="clear:both"></div>
    </div>
</div>    

<!-- BEGIN PAGE CONTENT -->
<div class="container reservation-page-wrap">
       <h4>Apartamento Luxo</h4>
    <div class="eleven columns">
        <br />	
        <!-- BEGIN ROOM CONTENT -->
        <div id="room-parametr">
            <div id="room-person">Máximo de hóspedes: <span class="icon-male"></span><span class="icon-male"></span><span class="icon-male"></span></div>
<!--            <div id="room-bed">1 Cama king</div>
            <div id="room-size">Dimensões: 80cm x 190cm</div>-->
        </div>
        <div id="room-main-description">Localizado na principal via de acesso de Teresina/PI-Brasil, a Avenida João XVIII, o Hotel Express fica próximo ao maior centro de conveções da região, assim como de casas de shows e restaurantes. 
            O Hotel Express reúne em sua estrutura a sofisticação de um hotel de primeira classe a um baixo custo, com a excelência do atendimento Arrey Hotels.

        </div>

        <div id="tabs-widget-wrap">
            <ul id="tabs">
                <li><a href="#" name="#tab1">Caracteristicas <span class="icon-star"></span></a></li>
                <li><a href="#" name="#tab2">Politicas <span class="icon-book"></span></a></li>
                <li><a href="#" name="#tab3">Observações <span class="icon-globe"></span></a></li> 
            </ul>
            <div id="tabs-content"> 
                <div id="tab1"><div id="room-features">
                        <ul class="room-features-list">
                            <li><span class="icon-star"></span>Café da manhã (*)</li>
                            <li><span class="icon-star"></span>Internet Grátis</li>
                            <li><span class="icon-star"></span>Ar-condicionado</li>
                            <li><span class="icon-star"></span>Canais a Cabo e TV tela plana</li>
                            <li><span class="icon-star"></span>Banheiro Privativo</li>
                            <li><span class="icon-star"></span>Telefone</li>
                            <li><span class="icon-star"></span>Ferro de Passar</li>
                            <li><span class="icon-star"></span>Frigobar vazio</li>
                            <li><span class="icon-star"></span>Estacionamento Privativo</li>
                            <li><span class="icon-star"></span>Arara e Quarda-roupa</li>
                            <li><span class="icon-star"></span>Mesa de trabalho</li>
                            <li><span class="icon-star"></span>Serviço despertar</li>
                            (*) Não incluso na diária
                        </ul>
                        <div style="clear:both"></div>
                    </div></div>
                <div id="tab2">
                    <div class="accordion-widget toggle">
                        <div class="accordion-header show"><a href="#"><span class="icon-plus"></span>Check-in </a></div>
                        <div class="accordion-content">Inicio as 12:00 horas</div>
                        <div class="accordion-header show"><a href="#"><span class="icon-plus"></span>Check-out</a></div>
                        <div class="accordion-content">Até 12:00 horas</div>
                        <div class="accordion-header"><a href="#"><span class="icon-plus"></span>Cancelamento / Pagamento</a></div>
                        <div class="accordion-content">
                            <p>Check-in ou check-out efetuado fora do horário será efetuado a cobrança de uma nova diária.</p>
                            <p>Reservas sem garantia são CANCELADAS às 18h do dia do check-in.</p>
                            <p>O não comparecimento sem comunicação prévia por escrito das reservas com garantia será considerado "no show" (desistência sem cancelamento) sendo cobrado o valor integral da primeira diária</p>
                        </div>
                        <div class="accordion-header"><a href="#"><span class="icon-plus"></span>Pets</a></div>
                        <div class="accordion-content">Animais de estimação não são permitidos.</div>
                        <div class="accordion-header"><a href="#"><span class="icon-plus"></span>Grupos</a></div>
                        <div class="accordion-content">Quando reservar mais de 5 quartos, condições diferentes e suplementos adicionais se aplicaram.</div>
                    </div>
                </div>
                <div id="tab3">
                     <div id="room-main-description">
                        <p>No momento do check-in, você deve apresentar um documento de identificação válido com foto para utilização de cartão de crédito. Por favor, observe que os pedidos especiais não podem ser garantidos, estão sujeitos à disponibilidade no momento do check-in e podem implicar em custos adicionais.</p>     
                        <p>Para nossos clientes, disponibilizamos máquinas de auto-atendimento em que poderá adquirir os itens de consumo que preferir para o seu frigobar durante sua estadia.</p>
                        <p>Apartamentos: Duplo, Duplo Casal e Single. Verifique a disponibilidade em nossos canais de atendimento</p>
                        <p>Não dispomos de apartamentos triplos e nem camas extras</p>
                        <p>Café da manhã não incluso no valor da diária</p>
                        <p>Não aceitamos cheques</p>
                    </div>
                </div>
            </div>
        </div>
        <br /><br />
        <!-- <div id="room-features">
                <ul class="room-features-list">
                        <li><span class="icon-star"></span>Free Wi-Fi Internet inside rooms</li>
                        <li><span class="icon-star"></span>Free Room Servises</li>
                        <li><span class="icon-star"></span>Breakfast included</li>
                        <li><span class="icon-star"></span>Beautiful view on the city</li>
                        <li><span class="icon-star"></span>Free Airport Pickup</li>
                        <li><span class="icon-star"></span>Free parking near hotel</li>
                        <li><span class="icon-star"></span>Satelite and Cable TV Included</li>
                        <li><span class="icon-star"></span>Air Conditioning</li>
                        <li><span class="icon-star"></span>Personal Safe</li>
                        <li><span class="icon-star"></span>Refrigerator</li>
                </ul>
                <div style="clear:both"></div>
        </div>
        <div id="frontend"></div> -->
        <!-- END ROOM CONTENT -->
    </div>

    <!-- BEGIN RESERVATION PAGE SIDEBAR -->
    <div class="four columns offset-by-one reservation-sidebar">



        <!-- BEGIN SIDEBAR CONTACT INFO -->
        <div class="sidebar-header">Para mais informações</div>
        <div id="contact-us-wrap">			
            <div id="contact-us-wrap-intro">Você esta com dúvidas ou com problemas com a reservas on-line? Entre em contato com um de nossos canais de atendimento.</div>
            <ul>
                <li id="by-phone">
                    <span class="icon-mobile-phone"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">Telefone</div> <br />+55(86)3216-6700</div>
                    <div style="clear:both"></div>
                </li>
                <li id="by-email">
                    <span class="icon-envelope-alt"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">email:</div> <br />reservas@executiveflat.com.br</div>
                    <div style="clear:both"></div>
                </li>
<!--                <li id="by-skype">
                    <span class="icon-skype"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">skype:</div> <br />hotelexpressarrey</div>
                    <div style="clear:both"></div>
                </li>-->
            </ul>
        </div>	
        <!-- END SIDEBAR CONTACT INFO -->

    </div>
    <!-- BEGIN RESERVATION PAGE SIDEBAR -->

</div>
<!-- END PAGE CONTENT -->