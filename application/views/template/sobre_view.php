<!-- BEGIN PAGE TITLE -->
<div id="top-content-divider">
    <div id="main-title-wrap">SOBRE O EXCUTIVE FLAT</div>
    <!--<div id="main-title-undertext">A sua localização privilegiada faz com que seja a melhor opção de hospedagem para viagens de negócios ou visitas à cidade de Teresina. </div>-->
</div>
<!-- END PAGE TITLE -->

<!-- BEGIN PAGE CONTENT -->
<div class="container">
    <div class="eleven columns page-wrap">
        <h2>Praticidade e economia se hospedam aqui</h2>
        <p style="text-align: justify;">O Hotel oferece 85 apartamentos Luxosos e completos, incluindo: televisão, ar-condicionado, frigobar. Temos todo o conforto e praticidade que você precisa durante a sua estadia em Teresina.</br>
        Oferecemos estacionamento privativo, wireless free, piscina e uma ótima localização. O restaurante Mediterrâneo conta com uma gastronomia internacional e nacional, para os amantes de uma boa culinária.</p> 
        <blockquote>A sua localização privilegiada faz com que seja a melhor opção de hospedagem para viagens de negócios ou visitas à cidade de Teresina. Próximo a shoppings, boates e diversos restaurantes. Uma variedade de opções para lazer, compras e alimentação esperando por você!</blockquote>
    </div>

    <!-- BEGIN PAGE SIDEBAR -->
    <div class="four columns offset-by-one page-sidebar">

        <!-- SIDEBAR SEARCH -->
        <!--<input type="text" value="Search ..." id="sidebar-search">-->

        <!-- BEGIN SIDEBAR POPULAR POSTS -->

<!--        <div class="sidebar-header">Notícias</div>
        <div id="popular-posts-wrap">
            <div class="popular-post-wrap">
                <img src="<?php //echo base_url() ?>assets/images/popular-post3.jpg" class="popular-post-image">
                <div class="popular-post-content-wrap">
                    <div class="popular-post-header">Lorem Ipsum Sit Doler</div>
                    <div class="popular-post-meta">20 Out, 2014 by Ralny Andrade</div>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="popular-post-wrap">
                <img src="<?php //echo base_url() ?>assets/images/popular-post1.jpg" class="popular-post-image">
                <div class="popular-post-content-wrap">
                    <div class="popular-post-header">Lorem Ipsum Sit Doler</div>
                    <div class="popular-post-meta">20 Out, 2014 by Ralny Andrade</div>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="popular-post-wrap">
                <img src="<?php //echo base_url() ?>assets/images/popular-post5.jpg" class="popular-post-image">
                <div class="popular-post-content-wrap">
                    <div class="popular-post-header">Lorem Ipsum Sit Doler</div>
                    <div class="popular-post-meta">20 Out, 2014 by Ralny Andrade</div>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>			-->

        <!-- END SIDEBAR POPULAR POSTS -->

        <!-- BEGIN SIDEBAR UPCOMMING EVENTS -->
        <!--
        <div class="sidebar-header">Upcomming Events</div>
        <div class="sidebar-events-wrap">
            <img src="images/event-sidebar1.jpg" class="popular-post-image">
            <div class="sidebar-events-header">
                2014 New Year Celebrations!
            </div>
            <div class="sidebar-events-meta">
                31 Dec, 2013 - 5 Jan, 2014
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="sidebar-events-wrap">
            <img src="images/event-sidebar2.jpg" class="popular-post-image">
            <div class="sidebar-events-header">
                2013 Christmas Celebrations!
            </div>
            <div class="sidebar-events-meta">
                25 Dec, 2013 - 29 Dec, 2013
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="sidebar-events-wrap">
            <img src="images/event-sidebar3.jpg" class="popular-post-image">
            <div class="sidebar-events-header">
                Nation Hotel Birthday!
            </div>
            <div class="sidebar-events-meta">
                10 Oct, 2013 - 13 Oct, 2013
            </div>
            <div style="clear:both"></div>
        </div>
        -->
        <!-- END SIDEBAR UPCOMMING EVENTS -->

        <!-- BEGIN SIDEBAR CONTACT INFO -->
        <div class="sidebar-header">Para mais informações</div>
        <div id="contact-us-wrap">			
            <div id="contact-us-wrap-intro">Você esta com dúvidas ou com problemas com a reservas on-line? Entre em contato com um de nossos canais de atendimento.</div>
            <ul>
                <li id="by-phone">
                    <span class="icon-mobile-phone"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">Telefone</div> <br />+55(86)3214-7050</div>
                    <div style="clear:both"></div>
                </li>
                <li id="by-email">
                    <span class="icon-envelope-alt"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">email:</div> <br />reservas@hotelexpressarrey.com.br</div>
                    <div style="clear:both"></div>
                </li>
<!--                <li id="by-skype">
                    <span class="icon-skype"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">skype:</div> <br />hotelexpressarrey</div>
                    <div style="clear:both"></div>
                </li>-->
            </ul>
        </div>	
        <!-- END SIDEBAR CONTACT INFO -->

    </div>
    <!-- END PAGE SIDEBAR -->

</div>
<!-- END PAGE CONTENT -->