<!-- BEGIN CONTACT PAGE GOOGLE MAPS -->
<div class="contact-maps-wrap">
    <div id="gmaps"></div>
</div>
<!-- END CONTACT PAGE GOOGLE MAPS -->

<!-- BEGIN CONTACT PAGE CONTENT -->
<div class="container">
    <div class="eleven columns contact-page" id="is-contact-page">
        <h3>Entre em contato</h3>
        <!--<div id="contact-form-intro">Donec non luctus turpis. Curabitur ut diam a turpis hendrerit aliquam. Nullam blandit bibendum turpis quis consequat. Nunc rhoncus neque ut quam venenatis consequat. Sed mollis facilisis consectetur. Curabitur purus ipsum, hendrerit id iaculis sed, congue facilisis dolor. Mauris aliquet tristique lacus vel pretium.</div>-->
       
        <!-- BEGIN CONTACT PAGE SEND MESSAGE FORM -->
         <form action="confirmacao/contato" method="post">
             <input type="text" id="name-comments-field" placeholder="Nome" name="nome" required>
             <input type="text" id="email-comments-field" placeholder="Email" name="email" required>
             <input type="text" id="phone-comments-field" placeholder="Telefone" name="fone" required>
            <div style="clear:both"></div>
            <textarea id="text-comments-field" name="mensagem" placeholder="Sua mensagem"></textarea>
            <button type="submit" class="room-reservation-select">Enviar Mensagem</button>
        </form>
        <!-- END CONTACT PAGE SEND MESSAGE FORM -->

    </div>

    <!-- BEGIN CONTACT PAGE SIDEBAR -->
    <div class="four columns offset-by-one contact-sidebar">

        <!-- SIDEBAR SEARCH -->
        <!--<input type="text" value="Search ..." id="sidebar-search">-->

        <!-- BEGIN SIDEBAR CONTACT INFO -->
        <div class="sidebar-header">Para mais informações</div>
        <div id="contact-us-wrap">			
            <div id="contact-us-wrap-intro">Você esta com dúvidas ou com problemas com a reservas on-line? Entre em contato com um de nossos canais de atendimento.</div>
            <ul>
                <li id="by-phone">
                    <span class="icon-mobile-phone"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">Telefone</div> <br />+55(86)3214-7050</div>
                    <div style="clear:both"></div>
                </li>
                <li id="by-email">
                    <span class="icon-envelope-alt"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">email:</div> <br />reservas@hotelexpressarrey.com.br</div>
                    <div style="clear:both"></div>
                </li>
                <li id="by-skype">
                    <span class="icon-skype"></span>
                    <div class="contact-info-content"><div class="contact-info-method-name">skype:</div> <br />hotelexpressarrey</div>
                    <div style="clear:both"></div>
                </li>
            </ul>
        </div>	
        <!-- END SIDEBAR CONTACT INFO -->


    </div>
</div>
<!-- END CONTACT PAGE CONTENT -->