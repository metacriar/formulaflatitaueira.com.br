<?php

class Usuario_model extends CI_Model{
    
    
    public function listaTodos(){
        
        $query = $this->db->query('SELECT * FROM usu_usuario');
    
        return $query->result();
                
    }
    
    public function insert($dados){
        
        $this->db->insert("usu_categoria", $dados);
        
    }
    
    public function insert2($dados){
        
        $this->db->insert_batch("usu_categoria", $dados);
        
    }
    
    public function update1($dados){
        $this->db->update("usu_categoria", $dados, "idCategoriaUsuario = 33");
    }
    
     public function update2($dados){
        
       $this->db->update_batch("usu_categoria", $dados, "idCategoriaUsuario");
        
    }
    
    
    public function delete1($dados){
        $this->db->delete("usu_categoria", $dados);
        
    }
    
}
