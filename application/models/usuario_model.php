<?php

class Usuario_model extends CI_Model{
    
    
    public function listaTodos(){
        
        $query = $this->db->get('usu_usuario');
    
        return $query->result();
                
    }
    
    public function se_exitir($atributo, $valor){
        
            $this->db->where($atributo, $valor);
            
            $query = $this->db->get('usu_categoria');
            
            if ($query->num_rows > 0){
                 return false;
            } else{
                 return true;
            }
        } 
  
    public function insert($dados){
        
        $this->db->insert("usu_categoria", $dados);
        
        return 'InsertRealizado';
        
    }
    
    public function insert2($dados){
        
        $this->db->insert_batch("usu_categoria", $dados);
        
        return 'InsertRealizado';
        
    }
    
    public function update1($dados){
        $this->db->update("usu_categoria", $dados, "idCategoriaUsuario = 33");
        
        return 'UpdateRealizado';
    }
    
     public function update2($dados){
        
       $this->db->update_batch("usu_categoria", $dados, "idCategoriaUsuario");
       
       return 'UpdateRealizado';
        
    }
    
    public function delete($dados){
        $this->db->delete("usu_categoria", $dados);
        
        return 'DeleteRealizado';
    
    }
    
    
    
    
    
}
