<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ContatoRealizado extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('url');

        //$this->_init($view = 'inc-content');
    }

    function _init() {
        $dados['title'] = 'Hotel Express Arrey';
        $this->load->view('template/inc-header', $dados);
        //$this->load->view('template/inc-navbar');
        //$this->load->view('template/inc-sidebar');
        $this->load->view('template/contato_realizado_view');
        $this->load->view('template/inc-footer');
    }

    function index() {
        $this->_init();
    }

    

}
