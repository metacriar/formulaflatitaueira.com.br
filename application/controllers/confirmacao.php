<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Confirmacao extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('html');

        //$this->_init($view = 'inc-content');
    }

    function _init() {
        $dados['title'] = 'Hotel Express Arrey';
        $this->load->view('template/inc-header', $dados);
        //$this->load->view('template/inc-navbar');
        //$this->load->view('template/inc-sidebar');
        $this->load->view('template/confirmacao_view');
        $this->load->view('template/inc-footer');
    }

    function index() {
        $this->_init();
    }

    function sendMail() {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'http://smtp.hotelexpressarrey.com.br',
            'smtp_port' => 587,
            'smtp_user' => 'reservas@hotelexpressarrey.com.br', // change it to yours
            'smtp_pass' => 'Arreyhotels2014', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        
        $mensagem = "
                     ------------------------------------------------------------------------
                     SOLICITAÇÃO DE RESERVAS
                     ------------------------------------------------------------------------
                     
                     DADOS PESSOAIS
                     ----------------
                     NOME: ".$this->input->post('nome')." ".$this->input->post('sobrenome')."   
                     EMAIL: ".$this->input->post('email')."
                     TELEFONE: ".$this->input->post('fone')."
                     CPF: ".$this->input->post('cpf')."  
                     RG: ".$this->input->post('rg')."
                     OBSERVAÇÕES: ".$this->input->post('obs')."
                     
                     DADOS DA RESERVA
                     ----------------
                     CHECK-IN: ".$this->input->post('datepickerin')."
                     CHECK-OUT: ".$this->input->post('datepickerout')."
                     QTD APARTAMENTOS: ".$this->input->post('qtdApart')."
                     QTD ADULTOS: ".$this->input->post('qtdAdulto')."
                     QTD CRIANÇAS: ".$this->input->post('qtdcrianca')."
                
                ";
                
        
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('reservas@hotelexpressarrey.com.br', 'Solicitação de Reserva'); // change it to yours
        $this->email->to('reservas@hotelexpressarrey.com.br'); // change it to yours
        $this->email->reply_to($this->input->post('email'));
        $this->email->subject('Hóspede: '. $this->input->post('nome').' '.$this->input->post('sobrenome'));
        $this->email->message(html_entity_decode($mensagem));
        if ($this->email->send()) {
            redirect('confirmacao');
        }
    }
    
    
    function contato() {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'http://smtp.hotelexpressarrey.com.br',
            'smtp_port' => 587,
            'smtp_user' => 'reservas@hotelexpressarrey.com.br', // change it to yours
            'smtp_pass' => 'Arreyhotels2014', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        
         $mensagem = "
                     ------------------------------------------------------------------------
                     FORMULARIO DE CONTATO
                     ------------------------------------------------------------------------
                     
                     DADOS PESSOAIS
                     ----------------
                     NOME: ".$this->input->post('nome')." ".$this->input->post('sobrenome')."   
                     EMAIL: ".$this->input->post('email')."
                     TELEFONE: ".$this->input->post('fone')."
                    
                     
                     MENSAGEM
                     ----------------
                     ".$this->input->post('mensagem')."
                ";

        
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('reservas@hotelexpressarrey.com.br', 'Formulário de Contato'); // change it to yours
        $this->email->to('reservas@hotelexpressarrey.com.br'); // change it to yours
        $this->email->reply_to($this->input->post('email'));
        $this->email->subject('Cliente: '. $this->input->post('nome').' '.$this->input->post('sobrenome'));
        $this->email->message(html_entity_decode($mensagem));
        if ($this->email->send()) {
            redirect('contatoRealizado');
        }
    }
    
    

}
