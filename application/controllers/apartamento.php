<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apartamento extends CI_Controller {
    
       
        function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		//$this->_init($view = 'inc-content');
	}
            
	function _init($view)
	{
	    $dados['title'] = 'Hotel Express Arrey';	
            $this->load->view('template/inc-header', $dados);
            //$this->load->view('template/inc-navbar');
            //$this->load->view('template/inc-sidebar');
            $this->load->view('template/'.$view);
            $this->load->view('template/inc-footer');
	}
        
        function index()
	{
		$this->_init('apartamentos_view');
             
	}
        

        function single_luxe()
	{
		$this->_init('single_luxe');
             
	}
        
         function double_luxe()
	{
		$this->_init('double_luxe');
             
	}
         function triple_luxe()
	{
		$this->_init('triple_luxe');
             
	}        

        
        
        
        
}
