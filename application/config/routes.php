<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['(:any)'] = "template/view/$1";
$route['default_controller'] = "home";
$route['apartamento'] = "apartamento";

$route['apartamento/single-luxe'] = "apartamento/single_luxe";
$route['apartamento/double-luxe'] = "apartamento/double_luxe";
$route['apartamento/triple-luxe'] = "apartamento/triple_luxe";

$route['apartamento/single-luxo'] = "apartamento/single_luxo";
$route['apartamento/double-luxo'] = "apartamento/double_luxo";
$route['apartamento/triplo-luxo'] = "apartamento/triple_luxo";

$route['reserva'] = "reserva";
$route['confirmacao'] = "confirmacao";
$route['checkout'] = "checkout";
$route['fotos'] = "fotos";
$route['sobre-o-hotel-express'] = "sobre";
$route['contato'] = "contato";
$route['confirmacao/sendMail'] = "confirmacao/sendMail";
$route['confirmacao/contato'] = "confirmacao/contato";
$route['contatoRealizado'] = "contatoRealizado";
$route['404_override'] = '';
$route['googlead4f0aafcb92f719.html'] = 'googlead4f0aafcb92f719';



/* End of file routes.php */
/* Location: ./application/config/routes.php */
